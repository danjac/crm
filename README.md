
Design Notes
============

This code uses the standard (3.7) Python library. No additional packages are used.

Service Layer
-------------

The models interact through a Service Layer [1]. The Service Layer provides a common set of interfaces we can reuse in a number of different situations, for example:

- a web service
- a command line interface (CLI)
- a task queue implementation such as Celery

The Service Layer also abstracts much of the underlying data storage and other systems, providing separation of concerns.

The models themselves are very lightweight, having only a few properties/methods managing their internal state.

Dependency Injection
--------------------

In addition to the Service Layer pattern, this codebase makes use of Dependency Injection (DI) [2]. Each Service class is initialized with a Store object; in a larger system we may also include, for example, an Mailer object or a TaskScheduler object. The object can be easily mocked out in unit testing: this is easier in many cases, and involves less "magic" than mocks. It also allows (to an extent) more flexibility in changing the underlying implementation: for example you could have a MySQL Store or PostgreSQL Store with the same API.

For simplicity here I've implemented a simple in-memory store subclassing a Python dict. Another simple implementation might write to a CSV, or a Redis store.

Things Not Covered
------------------

- Transactions
- Extensive validations (URL, email addresses, password length etc)

A real-world subscription system would probably archive old subscriptions for customer service, billing, legal and other reasons. There should also be separate entity for managing archived subscriptions. This is out of scope, but should be considered in a future implementation.

Running the Tests
-----------------

Code is written in Python 3.7. To run under pipenv:

> pipenv install

> pipenv run python -m unittest

References
----------

[1] https://en.wikipedia.org/wiki/Service_layer

[2] https://en.wikipedia.org/wiki/Dependency_injection

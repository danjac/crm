import datetime
import math
import unittest

from typing import Optional

from crm.exceptions import (
    AlreadySubscribed,
    InvalidAuth,
    LimitExceeded,
    NotSubscribed,
    ObjectDoesNotExist,
    SubscriptionExpired,
    UniqueExists,
)
from crm.models import Customer, Plan
from crm.services import CustomerService, PlanService, WebsiteService
from crm.stores.memory import (
    CustomerMemoryStore,
    PlanMemoryStore,
    WebsiteMemoryStore,
)

# Note: another approach here would be to mock the stores rather than
# use an in-memory implementation.


class TestWebsiteService(unittest.TestCase):
    def setUp(self):
        self.service = WebsiteService(WebsiteMemoryStore())

    def make_customer(
        self,
        expires: Optional[datetime.date] = None,
        plan: Optional[Plan] = None,
    ) -> Customer:
        customer = Customer(
            name="somebody", email="somebody@gmail.com", password="testpass"
        )
        customer.expires = expires
        customer.plan = plan
        return customer

    def make_plan(self, limit: int = 100) -> Plan:
        return Plan("Ultimate", 400, limit)

    def future_date(self) -> datetime.date:
        return datetime.date.today() + datetime.timedelta(days=500)

    def test_create_website_if_all_ok(self):
        customer = self.make_customer(
            expires=self.future_date(), plan=self.make_plan()
        )
        website = self.service.create_website(customer, "http://google.com")
        self.assertEqual(self.service.get_website(website.pk), website)

    def test_update_website(self):
        customer = self.make_customer(
            expires=self.future_date(), plan=self.make_plan()
        )
        website = self.service.create_website(customer, "http://google.com")
        website.url = "http://facebook.com"
        self.service.update_website(website)
        self.assertEqual(
            self.service.get_website(website.pk).url, "http://facebook.com"
        )

    def test_delete_website(self):
        customer = self.make_customer(
            expires=self.future_date(), plan=self.make_plan()
        )
        website = self.service.create_website(customer, "http://google.com")
        self.service.delete_website(website)
        self.assertRaises(
            ObjectDoesNotExist, self.service.get_website, website.pk
        )

    def test_create_website_if_unlimited_plan(self):
        customer = self.make_customer(
            expires=self.future_date(), plan=self.make_plan(limit=math.inf)
        )

        for i in range(100):
            self.service.create_website(customer, f"http://test-url-{i}.com")

        websites = self.service.get_websites_for_customer(customer)
        assert len(websites) == 100

    def test_create_website_if_url_exists(self):
        customer = self.make_customer(
            expires=self.future_date(), plan=self.make_plan()
        )
        self.service.create_website(customer, "http://google.com")

        self.assertRaises(
            UniqueExists,
            self.service.create_website,
            customer,
            "http://google.com",
        )

    def test_create_website_if_no_subscription(self):
        customer = self.make_customer(expires=self.future_date(), plan=None)
        self.assertRaises(
            NotSubscribed,
            self.service.create_website,
            customer,
            "http://google.com",
        )

    def test_create_website_if_subscription_expired(self):
        old_date = datetime.date.today() - datetime.timedelta(days=500)
        customer = self.make_customer(expires=old_date, plan=self.make_plan())
        self.assertRaises(
            SubscriptionExpired,
            self.service.create_website,
            customer,
            "http://google.com",
        )

    def test_create_website_if_limit_exceeded(self):
        customer = self.make_customer(
            expires=self.future_date(), plan=self.make_plan(limit=1)
        )
        self.service.create_website(customer, "http://facebook.com")

        self.assertRaises(
            LimitExceeded,
            self.service.create_website,
            customer,
            "http://google.com",
        )


class TestPlanService(unittest.TestCase):
    def setUp(self):
        self.service = PlanService(PlanMemoryStore())

    def create_plan(
        self, name: str = "Ultimate", price: int = 400, limit: int = 100
    ) -> Plan:
        return self.service.create_plan(name, price, limit)

    def test_create_and_get_plan(self):
        plan = self.create_plan()
        self.assertEqual(plan, self.service.get_plan(plan.pk))

    def test_create_plan_if_name_exists(self):
        self.create_plan()
        self.assertRaises(
            UniqueExists, self.service.create_plan, "Ultimate", 400, 100
        )


class TestCustomerService(unittest.TestCase):
    def setUp(self):
        self.service = CustomerService(CustomerMemoryStore())

    def create_customer(
        self,
        name: str = "somebody",
        email: str = "somebody@gmail.com",
        password: str = "testpass",
    ) -> Customer:
        return self.service.create_customer(name, email, password)

    def make_plan(
        self, name: str = "Premium", price: int = 100, limit: int = 100
    ) -> Plan:
        return Plan(name, price, limit)

    def test_create_and_get_customer(self):

        customer = self.create_customer()
        self.assertTrue(customer.pk)
        self.assertEqual(self.service.get_customer(customer.pk), customer)

    def test_get_customer_if_not_exists(self):

        self.assertRaises(
            ObjectDoesNotExist, self.service.get_customer, "12345"
        )

    def test_update_customer(self):
        customer = self.create_customer()
        customer.name = "new name"
        self.service.update_customer(customer)

        customer = self.service.get_customer(customer.pk)
        self.assertEqual(customer.name, "new name")

    def test_delete_customer(self):
        customer = self.create_customer()
        self.service.delete_customer(customer)
        self.assertRaises(
            ObjectDoesNotExist, self.service.get_customer, customer.pk
        )

    def test_create_if_email_exists(self):

        self.create_customer()

        self.assertRaises(
            UniqueExists,
            self.service.create_customer,
            "somebody else",
            "somebody@gmail.com",
            "testpass",
        )

    def test_authenticate_if_invalid_email(self):
        self.assertRaises(
            InvalidAuth,
            self.service.authenticate,
            "somebody@gmail.com",
            "testpass",
        )

    def test_authenticate_if_invalid_password(self):
        self.create_customer()
        self.assertRaises(
            InvalidAuth,
            self.service.authenticate,
            "somebody@gmail.com",
            "badpass",
        )

    def test_authenticate_if_ok(self):
        customer = self.create_customer()
        self.assertEqual(
            self.service.authenticate("somebody@gmail.com", "testpass"),
            customer,
        )

    def test_subscribe(self):
        customer = self.create_customer()
        self.service.subscribe(customer, self.make_plan())
        self.assertTrue(customer.expires)

    def test_subscribe_if_existing_plan(self):
        plan = self.make_plan()
        customer = self.create_customer()
        customer.plan = plan
        self.assertRaises(
            AlreadySubscribed, self.service.subscribe, customer, plan
        )

    def test_cancel_subscription(self):

        customer = self.create_customer()

        self.service.subscribe(customer, self.make_plan())
        self.service.cancel_subscription(customer)

        customer = self.service.get_customer(customer.pk)
        self.assertEqual(customer.plan, None)
        self.assertEqual(customer.expires, None)

    def test_renew_subscription(self):
        customer = self.create_customer()

        self.service.subscribe(customer, self.make_plan())

        old_expires = customer.expires
        self.service.renew_subscription(customer)
        self.assertTrue(customer.expires > old_expires)

    def test_renew_subscription_if_no_plan(self):
        customer = self.create_customer()
        self.assertRaises(
            NotSubscribed, self.service.renew_subscription, customer
        )

    def test_change_plan(self):
        customer = self.create_customer()

        old_plan = self.make_plan()
        self.service.subscribe(customer, old_plan)
        self.service.change_plan(customer, self.make_plan(name="Luxury"))
        self.assertNotEqual(customer.plan.name, old_plan.name)

    def test_change_plan_if_no_existing_plan(self):
        customer = self.create_customer()
        self.assertRaises(
            NotSubscribed, self.service.change_plan, customer, self.make_plan()
        )

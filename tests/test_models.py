import datetime
import unittest

from crm.models import Customer


class TestCustomer(unittest.TestCase):
    def setUp(self):
        self.customer = Customer(
            name="somebody", email="somebody@gmail.com", password="testpass"
        )

    def test_set_password(self):
        self.assertNotEqual(self.customer.password, "testpass")

    def test_check_password_correct(self):
        self.assertTrue(self.customer.check_password("testpass"))

    def test_check_password_incorrect(self):
        self.assertFalse(self.customer.check_password("something"))

    def test_has_expired_if_no_expiry_date(self):
        self.assertTrue(self.customer.has_expired)

    def test_has_expired_if_still_valid(self):
        self.customer.expires = datetime.date.today() + datetime.timedelta(
            days=500
        )
        self.assertFalse(self.customer.has_expired)

    def test_has_expired_if_no_longer_valid(self):
        self.customer.expires = datetime.date.today() - datetime.timedelta(
            days=500
        )
        self.assertTrue(self.customer.has_expired)

import datetime

from typing import List

from .exceptions import (
    AlreadySubscribed,
    InvalidAuth,
    LimitExceeded,
    NotSubscribed,
    ObjectDoesNotExist,
    SubscriptionExpired,
    UniqueExists,
)
from .models import Customer, Plan, Website
from .stores import CustomerStore, PlanStore, WebsiteStore
from .types import PK


class CustomerService:
    def __init__(self, store: CustomerStore):
        self.store = store

    def get_customer(self, pk: PK) -> Customer:
        """
        Returns a customer from the database. Raises
        ObjectDoesNotExist if not found.
        """
        return self.store.get(pk)

    def email_exists(self, email: str) -> bool:
        """
        Checks if (case-insensitive) match of email
        already in DB.
        """
        try:
            self.store.get_by_email(email)
            return True
        except ObjectDoesNotExist:
            return False

    def create_customer(
        self, name: str, email: str, password: str
    ) -> Customer:
        """
        Adds a new customer entity to the data store. A new
        primary key will be generated automatically by whatever
        database implementation.
        Email must be unique; raise UniqueExists exception
        if pre-existing email. Ensure value is lower-cased.
        """
        if self.email_exists(email):
            raise UniqueExists

        customer = Customer(name, email, password)
        customer.pk = self.store.create(customer)
        return customer

    def update_customer(self, customer: Customer):
        """
        Updates existing customer.
        Raises ObjectDoesNotExist if customer not yet in database.
        """
        self.store.update(customer)

    def delete_customer(self, customer: Customer):
        """
        Removes customer from database.

        Note: prior to deletion we would probably want to
        remove or disable all websites. In real-world
        situation this would probably be handled by a DB trigger
        (CASCADE) or some other event handling mechanism,
        or it may be managed manually.
        """
        self.store.delete(customer)

    def authenticate(self, email: str, password: str) -> Customer:
        """
        Authenticates the user against their email and (existing)
        password. Raises InvalidAuth exception if either user
        not found or password incorrect.
        """
        try:
            customer = self.store.get_by_email(email)
            if not customer.check_password(password):
                raise InvalidAuth
            return customer
        except ObjectDoesNotExist:
            raise InvalidAuth

    def subscribe(self, customer: Customer, plan: Plan):
        """
        Creates a new subscription for a given plan,
        with expiry date set a year from now.

        Raises AlreadySubscribed exception if there
        is a pre-existing subscription: either change
        plan or cancel the existing one first.
        """
        if customer.plan is not None:
            raise AlreadySubscribed
        customer.plan = plan
        customer.expires = datetime.date.today() + datetime.timedelta(days=365)
        self.update_customer(customer)

    def cancel_subscription(self, customer: Customer):
        """
        Removes the plan from the customer
        """
        customer.plan = None
        customer.expires = None
        self.update_customer(customer)

    def renew_subscription(self, customer: Customer):
        """
        Renews the customer expiration date by
        extending the date by a year.

        Raises NotSubscribed if no plan exists.
        """
        if customer.plan is None:
            raise NotSubscribed
        customer.expires = (
            customer.expires or datetime.date.today()
        ) + datetime.timedelta(days=365)
        self.update_customer(customer)

    def change_plan(self, customer: Customer, new_plan: Plan):
        """
        Upgrades/downgrades the customer to a new plan.

        The expiry date is unchanged, we just roll over to the
        current expiry date.

        Raises NotSubscribed if no plan available: use
        subscribe() for a new customer or one who has canceled their
        plan.
        """
        if customer.plan is None:
            raise NotSubscribed
        customer.plan = new_plan
        self.update_customer(customer)


class PlanService:
    """
    Note: for brevity for now you can only create a new plan,
    you can't change or delete existing plans.
    """

    def __init__(self, store: PlanStore):
        self.store = store

    def name_exists(self, name: str) -> bool:
        try:
            self.store.get_by_name(name)
            return True
        except ObjectDoesNotExist:
            return False

    def create_plan(self, name: str, price: int, limit: int) -> Plan:
        """
        Creates a new plan and adds it to the database.

        Plan name must be unique: trying
        """
        if self.name_exists(name):
            raise UniqueExists(f"Plan exists with name: {name}")
        plan = Plan(name, price, limit)
        plan.pk = self.store.create(plan)
        return plan

    def get_plan(self, pk: PK) -> Plan:
        """
        Retrieve plan from database. If no plan found raises
        ObjectDoesNotExist.
        """
        return self.store.get(pk)


class WebsiteService:
    def __init__(self, store: WebsiteStore):
        self.store = store

    def create_website(self, customer: Customer, url: str) -> Website:
        """
        Create a new website. We check the following:

        1. Customer has a valid subscription
        2. Their subscription has not expired.
        3. The customer has not exceeded their plan limit.
        4. The URL is unique.
        """
        if self.url_exists(url):
            raise UniqueExists(f"Website exists with URL: {url}")

        if customer.plan is None:
            raise NotSubscribed

        if customer.has_expired:
            raise SubscriptionExpired

        websites = self.get_websites_for_customer(customer)
        if len(websites) >= customer.plan.limit:
            raise LimitExceeded

        website = Website(customer, url)
        website.pk = self.store.create(website)
        return website

    def get_website(self, pk: PK) -> Website:
        """
        Fetches a website. Raises ObjectDoesNotExist if not found.
        """
        return self.store.get(pk)

    def url_exists(self, url: str) -> bool:
        """
        Checks if (case-insensitive) URL exists.
        """
        try:
            self.store.get_by_url(url)
            return True
        except ObjectDoesNotExist:
            return False

    def get_websites_for_customer(self, customer: Customer) -> List[Website]:
        """
        Return list of all websites belonging to a customer.
        """
        return self.store.fetch_by_customer(customer)

    def update_website(self, website):
        """
        Updates website details. Raises ObjectDoesNotExist if
        not found.
        """
        self.store.update(website)

    def delete_website(self, website: Website):
        """
        Deletes website.
        """
        self.store.delete(website)

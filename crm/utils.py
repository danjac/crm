import uuid


from typing import Any, List

from .exceptions import ObjectDoesNotExist
from .models import BaseModel
from .types import PK


class SimpleDictDB(dict):
    """
    A very simple in-memory store using plain Python dictionary.
    Useful for testing, but not intended for production!
    """

    def create(self, entity: BaseModel) -> PK:
        pk = str(uuid.uuid4())
        self[pk] = entity
        return pk

    def merge(self, entity: BaseModel):
        # "update" is a dict method...
        try:
            self[entity.pk] = entity
        except KeyError:
            raise ObjectDoesNotExist

    def delete(self, pk: PK):
        try:
            del self[pk]
        except KeyError:
            raise ObjectDoesNotExist

    def get_by_pk(self, pk: PK) -> BaseModel:
        try:
            return self[pk]
        except KeyError:
            raise ObjectDoesNotExist

    def get_by(self, attr: str, value: Any) -> BaseModel:
        for obj in self.values():
            if getattr(obj, attr) == value:
                return obj
        raise ObjectDoesNotExist

    def fetch_all(self) -> List[BaseModel]:
        return [obj for obj in self.values() if isinstance(obj, BaseModel)]

    def fetch_by(self, attr: str, value: Any) -> List[BaseModel]:
        return [obj for obj in self.fetch_all() if getattr(obj, attr) == value]

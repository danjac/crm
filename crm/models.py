import binascii
import datetime
import hashlib
import os


from typing import Optional

from .types import PK


class BaseModel:
    pk: Optional[PK] = None

    def __eq__(self, other: object) -> bool:
        if isinstance(other, BaseModel):
            return self.pk == other.pk
        return False


class Plan(BaseModel):

    name: str = ""
    price: int = 0
    limit: int = 0

    def __init__(self, name: str, price: int, limit: int):
        self.name = name
        self.price = price
        self.limit = limit


class Customer(BaseModel):
    name: str = ""
    email: str = ""
    encrypted_password: str = ""

    expires: Optional[datetime.date] = None
    plan: Optional[Plan] = None

    def __init__(self, name: str, email: str, password: str):
        self.name = name
        self.email = email
        self.password = password

    def __str__(self):
        return f"{self.name} ({self.email})"

    def __repr__(self):
        return f"<{str(self)}>"

    def check_password(self, password) -> bool:
        """
        Verifies password is correct.
        """
        if not self.password:
            return False

        try:
            salt = self.password[:64]
            stored_password = self.password[64:]
        except IndexError:
            return False

        hashed = binascii.hexlify(
            hashlib.pbkdf2_hmac(
                "sha512",
                password.encode("utf-8"),
                salt.encode("ascii"),
                100000,
            )
        ).decode("ascii")
        return hashed == stored_password

    @property
    def password(self) -> str:
        return self.encrypted_password

    @password.setter
    def password(self, password: str):
        # Note: this is probably not quite secure enough for production,
        # especially brute-force attacks. Use a vetted, secure solution.
        salt = hashlib.sha256(os.urandom(60)).hexdigest().encode("ascii")
        hashed = binascii.hexlify(
            hashlib.pbkdf2_hmac(
                "sha512", password.encode("utf-8"), salt, 100000
            )
        )
        self.encrypted_password = (salt + hashed).decode("ascii")

    @property
    def has_expired(self) -> bool:
        """
        Checks if the customer's subscription has expired.
        """
        return self.expires is None or datetime.date.today() > self.expires


class Website(BaseModel):
    customer: Optional[Customer] = None
    url: str = ""

    def __init__(self, customer: Customer, url: str):
        self.customer = customer
        self.url = url

class ObjectDoesNotExist(ValueError):
    ...


class InvalidAuth(ValueError):
    ...


class AlreadySubscribed(ValueError):
    ...


class NotSubscribed(ValueError):
    ...


class LimitExceeded(ValueError):
    ...


class SubscriptionExpired(ValueError):
    ...


class UniqueExists(ValueError):
    ...

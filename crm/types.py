"""
The Primary Key(PK) is abstracted into a custom type,
which allows us to reimplement in different ways e.g.
if we decide to move from a numerical autoincrement primary key
from an RDBMS to a GUID.
"""
PK = str

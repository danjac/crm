from abc import ABC, abstractmethod

from typing import List

from ..models import Customer, Plan, Website
from ..types import PK


class CustomerStore(ABC):
    @abstractmethod
    def get(self, id: PK) -> Customer:
        ...

    @abstractmethod
    def get_by_email(self, email: str) -> Customer:
        ...

    @abstractmethod
    def create(self, customer: Customer) -> PK:
        ...

    @abstractmethod
    def update(self, customer: Customer):
        ...

    @abstractmethod
    def delete(self, customer: Customer):
        ...


class PlanStore(ABC):
    @abstractmethod
    def create(self, plan: Plan) -> PK:
        ...

    @abstractmethod
    def get(self, pk: PK) -> Plan:
        ...

    @abstractmethod
    def get_by_name(self, name: str) -> Plan:
        ...


class WebsiteStore(ABC):
    @abstractmethod
    def create(self, website: Website) -> PK:
        ...

    @abstractmethod
    def update(self, website: Website):
        ...

    @abstractmethod
    def delete(self, website: Website):
        ...

    def get(self, pk: PK) -> Website:
        ...

    @abstractmethod
    def get_by_url(self, url: str) -> Website:
        ...

    @abstractmethod
    def fetch_by_customer(self, customer: Customer) -> List[Website]:
        ...

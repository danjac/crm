"""
In-memory data store implementations for testing purposes only!
"""
from typing import List

from . import CustomerStore, PlanStore, WebsiteStore
from ..exceptions import ObjectDoesNotExist
from ..models import Customer, Plan, Website
from ..types import PK
from ..utils import SimpleDictDB


class WebsiteMemoryStore(WebsiteStore):
    def __init__(self):
        self.db = SimpleDictDB()

    def create(self, website: Website) -> PK:
        return self.db.create(website)

    def update(self, website: Website):
        self.db.merge(website)

    def delete(self, website: Website):
        self.db.delete(website.pk)

    def get(self, pk: PK) -> Website:
        return self.db.get_by_pk(pk)

    def get_by_url(self, url: str) -> Website:
        url = url.lower()
        try:
            return [
                obj for obj in self.db.fetch_all() if obj.url.lower() == url
            ][0]
        except IndexError:
            raise ObjectDoesNotExist

    def fetch_by_customer(self, customer: Customer) -> List[Website]:
        return [
            obj
            for obj in self.db.fetch_all()
            if obj.customer.pk == customer.pk
        ]


class PlanMemoryStore(PlanStore):
    def __init__(self):
        self.db = SimpleDictDB()

    def create(self, plan: Plan) -> PK:
        return self.db.create(plan)

    def get(self, pk: PK) -> Plan:
        return self.db.get_by_pk(pk)

    def get_by_name(self, name: str) -> Plan:
        name = name.lower()
        try:
            return [
                obj for obj in self.db.fetch_all() if obj.name.lower() == name
            ][0]
        except IndexError:
            raise ObjectDoesNotExist


class CustomerMemoryStore(CustomerStore):
    """
    In-memory testing implementation of CustomerStore.
    """

    def __init__(self):
        self.db = SimpleDictDB()

    def create(self, customer: Customer) -> PK:
        return self.db.create(customer)

    def update(self, customer: Customer):
        self.db.merge(customer)

    def delete(self, customer: Customer):
        self.db.delete(customer.pk)

    def get(self, pk: PK) -> Customer:
        return self.db.get_by_pk(pk)

    def get_by_email(self, email: str) -> Customer:
        email = email.lower()
        try:
            return [
                obj
                for obj in self.db.fetch_all()
                if obj.email.lower() == email
            ][0]
        except IndexError:
            raise ObjectDoesNotExist
